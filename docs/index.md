# Let’s Roll!

The documentation structure is based on that
[excellent article from Divio](https://www.divio.com/en/blog/documentation/).

Please provide us any feedback as an early reader!


## [Tutorials](tutorials.md)

A tutorial:

* is learning-oriented
* allows the newcomer to get started
* is a lesson

*Analogy: teaching a small child how to cook*

* [Your first Roll application](tutorials.md#your-first-roll-application)
* [Your first Roll test](tutorials.md#your-first-roll-test)
* [Using extensions](tutorials.md#using-extensions)
* [Using events](tutorials.md#using-events)


## [How-to guides](how-to-guides.md)

A how-to guide:

* is goal-oriented
* shows how to solve a specific problem
* is a series of steps

*Analogy: a recipe in a cookery book*

* [How to install Roll](how-to-guides.md#how-to-install-roll)
* [How to create an extension](how-to-guides.md#how-to-create-an-extension)
* [How to return an HTTP error](how-to-guides.md#how-to-return-an-http-error)
* [How to return JSON content](how-to-guides.md#how-to-return-json-content)
* [How to subclass Roll itself](how-to-guides.md#how-to-subclass-roll-itself)
* [How to deploy Roll into production](how-to-guides.md#how-to-deploy-roll-into-production)


## [Discussions](discussions.md)

A discussion:

* is understanding-oriented
* explains
* provides background and context

*Analogy: an article on culinary social history*

* [Why Roll?](discussions.md#why-roll)


## [Reference](reference.md)

A reference guide:

* is information-oriented
* describes the machinery
* is accurate and complete

*Analogy: a reference encyclopaedia article*

* [Core objects](reference.md#core-objects)
* [Extensions](reference.md#extensions)
* [Events](reference.md#events)


## [Changelog](changelog.md)

A changelog:

* is breaking-change-oriented
* links to related issues
* is concise

*Analogy: git blame :p*
